package com.saad.sms;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;




public class sms  {

	private JFrame frmSuveryourManagementSystem;
	private JTextField namet;
	private JTextField cart;
	private JTextField datet;
	private JTextField instypet;
	private JTextField regdatet;
	private JTextField regnot;
	private JLabel lblPhoneNo;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JTextField imgpath2;
	private JTextField imgpath3;
	private JTextField imgpath1;
	private JTextField phonet;
	private JButton btnNewButton;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch(Exception e) {
			  System.out.println("Error setting native LAF: " + e);
			}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					sms window = new sms();
					window.frmSuveryourManagementSystem.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public sms() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		class JButtonStateController implements DocumentListener {
			 JButton send;
			  
			  JButtonStateController(JButton send) {
			     this.send = send ;
			  }

			  public void changedUpdate(DocumentEvent e) {
			    disableIfEmpty(e);
			  }

			  public void insertUpdate(DocumentEvent e) {
			    disableIfEmpty(e);
			  }

			  public void removeUpdate(DocumentEvent e) {
			    disableIfEmpty(e);
			  }
			  

			  public void disableIfEmpty(DocumentEvent e) {
			    send.setEnabled(e.getDocument().getLength() > 0);
			    
			  }
			}
		frmSuveryourManagementSystem = new JFrame();
		frmSuveryourManagementSystem.setResizable(false);
		frmSuveryourManagementSystem.setTitle("Suveryour Management System");
		frmSuveryourManagementSystem.setBounds(100, 100, 878, 531);
		frmSuveryourManagementSystem.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSuveryourManagementSystem.getContentPane().setLayout(null);
		
		final JPanel panel_1 = new JPanel();
		panel_1.setForeground(Color.BLACK);
		panel_1.setBounds(50, 5, 764, 72);
		frmSuveryourManagementSystem.getContentPane().add(panel_1);
		
		JLabel head = new JLabel("Surveyour Management System");
		head.setFont(new Font("Levenim MT", Font.BOLD, 34));
		panel_1.add(head);
		
		final JPanel panel_2 = new JPanel();
		panel_2.setBounds(50, 107, 397, 375);
		frmSuveryourManagementSystem.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblCustomerName = new JLabel("Customer Name: ");
		lblCustomerName.setBounds(0, 25, 107, 14);
		lblCustomerName.setFont(new Font("Levenim MT", Font.BOLD, 11));
		panel_2.add(lblCustomerName);
		
		namet = new JTextField();
		namet.setBounds(119, 22, 173, 19);
		panel_2.add(namet);
		namet.setColumns(10);
		
		cart = new JTextField();
		cart.setBounds(119, 83, 173, 19);
		panel_2.add(cart);
		cart.setColumns(10);
		
		datet = new JTextField();
		datet.setBounds(119, 114, 173, 19);
		panel_2.add(datet);
		datet.setColumns(10);
		
		instypet = new JTextField();
		instypet.setBounds(119, 148, 173, 19);
		panel_2.add(instypet);
		instypet.setColumns(10);
		
		regdatet = new JTextField();
		regdatet.setBounds(119, 179, 173, 19);
		panel_2.add(regdatet);
		regdatet.setColumns(10);
		
		regnot = new JTextField();
		regnot.setBounds(119, 210, 173, 19);
		panel_2.add(regnot);
		regnot.setColumns(10);
		
		lblPhoneNo = new JLabel("Phone No:");
		lblPhoneNo.setBounds(0, 55, 93, 14);
		lblPhoneNo.setFont(new Font("Levenim MT", Font.BOLD, 11));
		panel_2.add(lblPhoneNo);
		
		lblNewLabel = new JLabel("Car Model:");
		lblNewLabel.setBounds(0, 86, 93, 14);
		lblNewLabel.setFont(new Font("Levenim MT", Font.BOLD, 11));
		panel_2.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Accident Date:");
		lblNewLabel_1.setBounds(0, 117, 93, 14);
		lblNewLabel_1.setFont(new Font("Levenim MT", Font.BOLD, 11));
		panel_2.add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("Insurance Type:");
		lblNewLabel_2.setBounds(0, 151, 109, 14);
		lblNewLabel_2.setFont(new Font("Levenim MT", Font.BOLD, 11));
		panel_2.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Registered Since:");
		lblNewLabel_3.setBounds(0, 182, 109, 14);
		lblNewLabel_3.setFont(new Font("Levenim MT", Font.BOLD, 11));
		panel_2.add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("Registeration No:");
		lblNewLabel_4.setBounds(0, 213, 109, 14);
		lblNewLabel_4.setFont(new Font("Levenim MT", Font.BOLD, 11));
		panel_2.add(lblNewLabel_4);
		
		JButton send = new JButton("Send");
		send.setBounds(119, 255, 176, 38);
		send.setEnabled(false);
		panel_2.add(send);
		
		phonet = new JTextField();
		phonet.setBounds(119, 52, 173, 20);
		panel_2.add(phonet);
		phonet.setColumns(10);
		
		final JPanel panel_3 = new JPanel();
		panel_3.setBounds(479, 107, 373, 375);
		frmSuveryourManagementSystem.getContentPane().add(panel_3);
		panel_3.setLayout(null);
		
		
		
		
		
		send.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String path1=imgpath1.getText();
				String path2=imgpath2.getText();
				String path3=imgpath3.getText();
				
				String getName = namet.getText();
				String getPhone = phonet.getText();
				String getRegdate = regdatet.getText();
				String getRegno = regnot.getText();
				String getInstype = instypet.getText();
				String getDate = datet.getText();
				String getCar = cart.getText();
				
				Connection conn = null;
				Connection con = null;
				Statement st = null;
				 Statement ctTB=null;
				 Statement ctDB=null;
				 
				  String driver = "com.mysql.jdbc.Driver";
				  String sql = "INSERT INTO project(customername,phone,regno,rdate,carmodel,instype,acc_date,imgpath1,imgpath2,imgpath3)"+
				  "VALUES('"+getName+"','"+getPhone+"','"+getRegno+"','"+getRegdate+"','"+getCar+"','"+getInstype+"','"+getDate+"','"+path1+"','"+path2+"','"+path3+"')";
				  String createTable = "CREATE TABLE IF NOT EXISTS `project` " +
				  		"(`customername` varchar(250) NOT NULL," +
				  		"`phone` varchar(20) NOT NULL," +
				  		"`carmodel` varchar(100) NOT NULL," +
				  		"`acc_date` varchar(250) NOT NULL,`instype`" +
				  		" varchar(20) NOT NULL," +
				  		"`rdate` varchar(250) NOT NULL," +
				  		"`regno` varchar(10) NOT NULL, " +
				  		" `imgpath1` longtext NOT NULL," +
				  		"`imgpath2` longtext NOT NULL," +
				  		"`imgpath3` longtext NOT NULL)";
				  String createDB = "CREATE DATABASE IF NOT EXISTS `project`";
				  try{
					  
					  try{
					  Class.forName(driver);
					  con = (Connection) DriverManager.getConnection(
							    "jdbc:mysql://sql2.freemysqlhosting.net/sql26204",
							    "sql26204", "dE5*aM7!");}
					  catch(ClassNotFoundException e) {
								JOptionPane.showMessageDialog(null,"MySQL drivers not found."+e.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);
								e.printStackTrace();}
					  try {
						  
						  
						  ctDB = (Statement) con.createStatement();
						  ctDB.executeUpdate(createDB);
						  
					  }
					  catch(SQLException ctDBex){
						  	
						 // JOptionPane.showMessageDialog(null,"SQL statement is not executed!"+ctDBex.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);
					  }
				
					  try {
						  conn = (Connection) DriverManager.getConnection("jdbc:mysql://sql2.freemysqlhosting.net/sql26204",
								    "sql26204", "dE5*aM7!");
								   
						
						  ctTB = (Statement) conn.createStatement();
						  ctTB.execute(createTable);
					  }
					  catch(SQLException ctTBex){
						  	
						  JOptionPane.showMessageDialog(null,"SQL statement is not executed!\n\n"+ctTBex.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);
					  }
					  
					  try{
						  conn = (Connection) DriverManager.getConnection("jdbc:mysql://sql2.freemysqlhosting.net/sql26204",
								    "sql26204", "dE5*aM7!");
					  st = (Statement) conn.createStatement();
					  st.execute(sql);
					  conn.close();
					  JOptionPane.showMessageDialog(null,"Information recorded successfully!");
					  }
					  catch (SQLException sqle){
					  JOptionPane.showMessageDialog(null,"SQL statement could not executed!\n\n"+sqle.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);
					  }
					  }
					  catch (Exception e){
					  e.printStackTrace();
					  JOptionPane.showMessageDialog(null,"Something went wrong!\n\n"+e.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);
					  }
				  
			}
			
		});
		
		
		
		imgpath2 = new JTextField();
		imgpath2.setBounds(106, 82, 132, 23);
		panel_3.add(imgpath2);
		imgpath2.setColumns(10);
		
		imgpath3 = new JTextField();
		imgpath3.setBounds(106, 126, 132, 23);
		panel_3.add(imgpath3);
		imgpath3.setColumns(10);
		
		JButton imgb1 = new JButton("Browse");
		imgb1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 JFileChooser fileChooser = new JFileChooser();
				 
			        
			        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);	 
			        fileChooser.setAcceptAllFileFilterUsed(false);
			 
			        int rVal = fileChooser.showOpenDialog(null);
			        if (rVal == JFileChooser.APPROVE_OPTION) {
			          imgpath1.setText(fileChooser.getSelectedFile().toString());
			        }
			}
		});
		imgb1.setBounds(247, 34, 89, 23);
		panel_3.add(imgb1);
		
		JButton imgb2 = new JButton("Browse");
		imgb2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 JFileChooser fileChooser = new JFileChooser();
				 
			        
			        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);	 
			        fileChooser.setAcceptAllFileFilterUsed(false);
			 
			        int rVal = fileChooser.showOpenDialog(null);
			        if (rVal == JFileChooser.APPROVE_OPTION) {
			          imgpath2.setText(fileChooser.getSelectedFile().toString());
			        }
			}
		});
		imgb2.setBounds(248, 81, 89, 23);
		panel_3.add(imgb2);
		
		JButton imgb3 = new JButton("Browse");
		imgb3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 JFileChooser fileChooser = new JFileChooser();
				 
			        
			        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);	 
			        fileChooser.setAcceptAllFileFilterUsed(false);
			 
			        int rVal = fileChooser.showOpenDialog(null);
			        if (rVal == JFileChooser.APPROVE_OPTION) {
			          imgpath3.setText(fileChooser.getSelectedFile().toString());
			        }
			}
		});
		imgb3.setBounds(248, 125, 89, 23);
		panel_3.add(imgb3);
		
		JLabel lblNewLabel_5 = new JLabel("Image 1:");
		lblNewLabel_5.setFont(new Font("Levenim MT", Font.BOLD, 11));
		lblNewLabel_5.setBounds(26, 38, 70, 14);
		panel_3.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Image 2:");
		lblNewLabel_6.setFont(new Font("Levenim MT", Font.BOLD, 11));
		lblNewLabel_6.setBounds(26, 85, 70, 14);
		panel_3.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Image 3:");
		lblNewLabel_7.setFont(new Font("Levenim MT", Font.BOLD, 11));
		lblNewLabel_7.setBounds(26, 129, 70, 14);
		panel_3.add(lblNewLabel_7);
		
		imgpath1 = new JTextField();
		imgpath1.setBounds(106, 35, 132, 23);
		panel_3.add(imgpath1);
		imgpath1.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 88, 842, 8);
		frmSuveryourManagementSystem.getContentPane().add(separator);
		
		Document document = regnot.getDocument();
		document.addDocumentListener(new JButtonStateController(send));
		
		btnNewButton = new JButton("Check records");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Records rec = new Records();
				rec.records.setVisible(true);
				rec.records.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				
			}
		});
		btnNewButton.setBounds(119, 304, 173, 38);
		panel_2.add(btnNewButton);
		
	
		
	}
}
