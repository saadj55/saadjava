package com.saad.sms;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;


public class Records{

	JFrame records;
	private JPanel panel;
	private JLabel label;
	private JSeparator separator;
	private JPanel panel_1;
	private JLabel lblEnterCustomerName;
	private JTextField cname;
	private JTextField car;
	private JTextField instype;
	private JTextField date;
	private JTextField regdate;
	private JTextField regno;
	private JLabel label_2;
	private JLabel label_3;
	private JLabel label_4;
	private JLabel label_5;
	private JLabel label_6;
	private JLabel label_7;
	private JTextField phone;
	private JSeparator separator_1;
	private JButton fetch;

	/**
	 * @wbp.parser.entryPoint
	 */
	public Records() {
		records = new JFrame();
		records.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		records.setBounds(100, 100, 878, 531);
		records.getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setForeground(Color.BLACK);
		panel.setBounds(86, 0, 764, 72);
		records.getContentPane().add(panel);
		
		label = new JLabel("Surveyour Management System");
		label.setFont(new Font("Levenim MT", Font.BOLD, 34));
		panel.add(label);
		
		separator = new JSeparator();
		separator.setBounds(12, 80, 842, 8);
		records.getContentPane().add(separator);
		
		panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBounds(105, 100, 696, 375);
		records.getContentPane().add(panel_1);
		
		lblEnterCustomerName = new JLabel("Enter Customer Name: ");
		lblEnterCustomerName.setFont(new Font("Levenim MT", Font.BOLD, 11));
		lblEnterCustomerName.setBounds(102, 25, 150, 14);
		panel_1.add(lblEnterCustomerName);
		
		cname = new JTextField();
		cname.setColumns(10);
		cname.setBounds(259, 20, 173, 19);
		panel_1.add(cname);
		
		car = new JTextField();
		car.setEditable(false);
		car.setColumns(10);
		car.setBounds(259, 125, 173, 19);
		panel_1.add(car);
		
		instype = new JTextField();
		instype.setEditable(false);
		instype.setColumns(10);
		instype.setBounds(259, 156, 173, 19);
		panel_1.add(instype);
		
		date = new JTextField();
		date.setEditable(false);
		date.setColumns(10);
		date.setBounds(259, 187, 173, 19);
		panel_1.add(date);
		
		regdate = new JTextField();
		regdate.setEditable(false);
		regdate.setColumns(10);
		regdate.setBounds(259, 218, 173, 19);
		panel_1.add(regdate);
		
		regno = new JTextField();
		regno.setEditable(false);
		regno.setColumns(10);
		regno.setBounds(259, 249, 173, 19);
		panel_1.add(regno);
		
		label_2 = new JLabel("Phone No:");
		label_2.setFont(new Font("Levenim MT", Font.BOLD, 11));
		label_2.setBounds(143, 98, 93, 14);
		panel_1.add(label_2);
		
		label_3 = new JLabel("Car Model:");
		label_3.setFont(new Font("Levenim MT", Font.BOLD, 11));
		label_3.setBounds(143, 130, 93, 14);
		panel_1.add(label_3);
		
		label_4 = new JLabel("Accident Date:");
		label_4.setFont(new Font("Levenim MT", Font.BOLD, 11));
		label_4.setBounds(143, 192, 93, 14);
		panel_1.add(label_4);
		
		label_5 = new JLabel("Insurance Type:");
		label_5.setFont(new Font("Levenim MT", Font.BOLD, 11));
		label_5.setBounds(143, 161, 109, 14);
		panel_1.add(label_5);
		
		label_6 = new JLabel("Registered Since:");
		label_6.setFont(new Font("Levenim MT", Font.BOLD, 11));
		label_6.setBounds(143, 223, 109, 14);
		panel_1.add(label_6);
		
		label_7 = new JLabel("Registeration No:");
		label_7.setFont(new Font("Levenim MT", Font.BOLD, 11));
		label_7.setBounds(143, 254, 109, 14);
		panel_1.add(label_7);
		
		phone = new JTextField();
		phone.setEditable(false);
		phone.setColumns(10);
		phone.setBounds(259, 93, 173, 20);
		panel_1.add(phone);
		
		separator_1 = new JSeparator();
		separator_1.setBounds(12, 50, 672, 9);
		panel_1.add(separator_1);
		
		fetch = new JButton("Fetch");
		fetch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cusname = cname.getText();
				String phoneq = "SELECT phone FROM project WHERE customername= '"+cusname+"'";
				String carq = "SELECT carmodel FROM project WHERE customername= '"+cusname+"'";
				String instypeq = "SELECT instype FROM project WHERE customername= '"+cusname+"'";
				String dateq = "SELECT acc_date FROM project WHERE customername= '"+cusname+"'";
				String regdateq = "SELECT rdate FROM project WHERE customername= '"+cusname+"'";
				String regnoq = "SELECT regno FROM project WHERE customername= '"+cusname+"'";
				
				Connection con = null;
				Statement st1 = null;
				Statement st2 = null;
				Statement st3 = null;
				Statement st4 = null;
				Statement st5 = null;
				Statement st6 = null;
				ResultSet rs = null;
				String driver = "com.mysql.jdbc.Driver";
				try {
					
					try{
						  Class.forName(driver);
						  con = (Connection) DriverManager.getConnection(
								    "jdbc:mysql://sql2.freemysqlhosting.net/sql26204",
								    "sql26204", "dE5*aM7!");
						
						}
						  catch(ClassNotFoundException classex) {
									JOptionPane.showMessageDialog(null,"MySQL drivers not found."+classex.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);
									classex.printStackTrace();
									}
					
					try{
						
						st1 = (Statement) con.createStatement();
						rs = st1.executeQuery(phoneq);
						if (rs.next()) {
			                phone.setText(rs.getString(1));
			            }
						st2 = (Statement) con.createStatement();
						rs = st2.executeQuery(carq);
						if (rs.next()) {
			                car.setText(rs.getString(1));
			            }
						st3 = (Statement) con.createStatement();
						rs = st3.executeQuery(instypeq);
						if (rs.next()) {
			                instype.setText(rs.getString(1));
			            }
						st4 = (Statement) con.createStatement();
						rs = st4.executeQuery(dateq);
						if (rs.next()) {
			                date.setText(rs.getString(1));
			            }
						st5 = (Statement) con.createStatement();
						rs = st5.executeQuery(regdateq);
						if (rs.next()) {
			                regdate.setText(rs.getString(1));
			            }
						st6 = (Statement) con.createStatement();
						rs = st6.executeQuery(regnoq);
						if (rs.next()) {
			                regno.setText(rs.getString(1));
			            }
						
					}
					catch(SQLException sqlex){
					JOptionPane.showMessageDialog(null, sqlex.getMessage());
						
					}
					
					
				}
				catch(Exception ex){
					JOptionPane.showMessageDialog(null,ex.getMessage());	
			
				}
				
				
				
				
			}
		});
		fetch.setToolTipText("Click to fetch data");
		fetch.setBounds(444, 17, 98, 26);
		panel_1.add(fetch);
		records.setVisible(true);
		
	}
}
